const assert = require('assert');
const {URL} = require('url');
const path = require('path');
const expect = require('chai').expect;

function login () {
    const emailField = $('input[name=email]');
    const passField = $('input[type=password]');
    const loginButton = $('button.button.is-primary');

    emailField.setValue('rinasaara@gmail.com');
    passField.setValue('Qwerty1234');
    loginButton.click();


    const headerDropDown = $('div.profile.navbar-link.navbar-dropdown-menu');
    expect($('.navbar-end').isDisplayed());
    headerDropDown.waitForDisplayed(10000);
}

describe('test hedonist', () => {

    beforeEach(function() {
        browser.url('https://staging.bsa-hedonist.online');
        $('#app').waitForDisplayed();
    });

    xit('log in hedonist', () => {

        login();
        const url= new URL(browser.getUrl());
        const actualUrl = url.hostname.toString() + url.pathname.toString();
        assert.equal(actualUrl, "staging.bsa-hedonist.online/search");
    });

    xit('registering in hedonist', () => {
        browser.url('https://staging.bsa-hedonist.online/signup');

        const firstNameField = $('input[name=firstName]');
        const lastNameField = $('input[name=lastName]');
        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');

        firstNameField.setValue('Tina');
        lastNameField.setValue('Chan');
        emailField.setValue('rinasaara99@gmail.com');
        passField.setValue('Qwerty1234');
        loginButton.click();

        browser.pause(10000);

        const url= new URL(browser.getUrl());
        const actualUrl = url.hostname.toString() + url.pathname.toString();
        assert.equal(actualUrl, "staging.bsa-hedonist.online/login")
    });
    xit('log in hedonist with invalid data', () => {

        browser.url('https://staging.bsa-hedonist.online');

        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');

        emailField.setValue('qwertty@gma.com');
        passField.setValue('Qwerty1234');
        loginButton.click();

        browser.pause(10000);

    });

    it('create new place', () => {
        login();
        browser.url('https://staging.bsa-hedonist.online/places/add');

        const namePlaceField  = $('input.input.is-medium');
        //const cityField  = $$('input[placeholder="Location"]')[1];
        const zipField = $('input[placeholder="09678"]');
        const adressField = $('input[placeholder="Khreschatyk St., 14"]');
        const phoneField = $('input[placeholder="+380961112233"]');
        const siteField = $('input[placeholder="http://the-best-place.com/"]');
        const descriptionField = $('textarea.textarea');
        const buttonNext = $('#app > div.section > div > div > section > div:nth-child(1) > div.buttons.is-centered > span');

        namePlaceField.setValue('Name 1');
        //cityField.setValue('Kiev');
        zipField.setValue('09678');
        adressField.setValue('Test adress');
        phoneField.setValue('+38909994949444');
        siteField.setValue('https://study.binary-studio.com/');
        descriptionField.setValue('We are the Best place, that you could visit..');
        buttonNext.click();

        const uploadControl = $('input[type=file]');
        browser.pause(5000);
        //
        // const filePath = path.resolve(path.join(__dirname, 'image', '1.png'));
        const filePath = path.join(__dirname, '5.png');
        console.log("WWWWWWWWWWWWWWWWWWWW", filePath);
        const remoteFilePath = browser.uploadFile(filePath);
        uploadControl.setValue("C:\\Users\\Owner\\bs-academy-2019-yeremenko_arina\\BSA-2019-wdio\\specs\\5.png");
        // const uploadFileField = $('input[type="button"]');

        // const filePath = path.join('Users', 'Owner', 'WebstormProjects', 'bs-academy-2019-yeremenko_arina', '1.png');
        // console.log(filePath);
        // const remoteFilePath = browser.uploadFile(filePath);
        // uploadFileField.setValue(remoteFilePath);

    });
    xit('create new list', () => {
        login();
        browser.url('https://staging.bsa-hedonist.online/my-lists/add');

        //const buttonAddnewList = $('#app > section > div.add-list.has-text-right > a');
        const listNameField = $('#list-name');
        const buttonSaveList = $('button.is-success');
        const locationField = $ ('div.search-inputs__location > div > div > div > div.control.is-clearfix > input');
        const searchPlaceField = $('input.search-field.input');
        const choicePlaceLI = $('div.search-places__list > ul > li');


        listNameField.setValue('Name list' + ' ' + Math.round(Math.random() * 500));
        searchPlaceField.setValue('Red Cat');
        choicePlaceLI.click();

        buttonSaveList.click();
        browser.pause(10000);
    });
    xit('delete list', () => {
        login();
        browser.url('https://staging.bsa-hedonist.online/my-lists');
        const arrayPlacediv  = $$('div.container.place-item');
        const randomIndexPlace = Math.floor(Math.random() * arrayPlacediv.length);

        browser.pause(5000);

        $$('button.button.is-danger')[randomIndexPlace].click();
        const modalWindow = $('div.animation-content.modal-content');
        modalWindow.waitForDisplayed(10000);

        $('div.buttons.is-centered > button.button.is-danger').click();

        const mainContainer = $('section.container');
        mainContainer.waitForDisplayed(10000);

        browser.pause(5000);
    });
});